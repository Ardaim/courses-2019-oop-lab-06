package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends IllegalStateException {
	
	private static final long serialVersionUID = -6141526663394301859L;
	private final double current;
	private final double cost;

	public NotEnoughBatteryException(String s, double currentBatteryLv, double batteryCost) {
		super(s);
		this.current=currentBatteryLv;
		this.cost=batteryCost;

	}

	@Override
	public String toString() {
		return "NotEnoughBatteryException [current battery level =" + current + ", expected battery level =" + cost + "]";
	}

	

}
