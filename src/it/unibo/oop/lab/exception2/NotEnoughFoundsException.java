package it.unibo.oop.lab.exception2;

public class NotEnoughFoundsException extends IllegalStateException {

	private static final long serialVersionUID = 7147584031118898730L;

	public NotEnoughFoundsException() {
		// TODO Auto-generated constructor stub
	}

	public NotEnoughFoundsException(String s) {
		super(s);
		// TODO Auto-generated constructor stub
	}

	public NotEnoughFoundsException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public NotEnoughFoundsException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
