package it.unibo.oop.lab.exception2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * JUnit test to test {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

	/**
	 * Used to test Exceptions on {@link StrictBankAccount}.
	 */
	@Test
	public void testBankOperations() {
		/*
		 * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a scelta,
		 * con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
		 * 
		 * 2) Effetture un numero di operazioni a piacere per verificare che le
		 * eccezioni create vengano lanciate soltanto quando opportuno, cioè in presenza
		 * di un id utente errato, oppure al superamento del numero di operazioni ATM
		 * gratuite.
		 */
		final var usr1 = new AccountHolder("mario", "rossi", 1);
		final var usr2 = new AccountHolder("luigi", "verdi", 2);
		final var acc1 = new StrictBankAccount(usr1.getUserID(), 10000, 10);
		final var acc2 = new StrictBankAccount(usr2.getUserID(), 10000, 10);

		System.out.println(acc1.getNTransactions());
		for (int i = 0; i < 10; i++) {
			try {
				acc1.depositFromATM(usr1.getUserID(), 1);
				System.out.println(acc1.getNTransactions());
			} catch (TransactionsOverQuotaException e) {
				fail();
			}

		}
		try {
			acc1.depositFromATM(usr1.getUserID(), 1);
			fail();
		} catch (TransactionsOverQuotaException e) {
			System.out.println(acc1.getNTransactions());
			assertNotNull(e.getMessage());
		}

		try {
			acc1.deposit(usr1.getUserID(), 10);

		} catch (WrongAccountHolderException e) {
			fail();
		}
		try {
			acc1.deposit(usr2.getUserID(), 10);
			fail();
		} catch (WrongAccountHolderException e) {
			assertNotNull(e.getMessage());
		}

		try {
			acc1.withdraw(usr1.getUserID(), 999999);
			fail();

		} catch (NotEnoughFoundsException e) {
			assertNotNull(e.getMessage());
		}

		try {
			acc1.withdraw(usr1.getUserID(), 10009.0);
			System.out.print(acc1.getBalance());

		} catch (NotEnoughFoundsException e) {
			System.out.print(acc1.getBalance());
			fail();

		}

		try {
			acc2.computeManagementFees(usr1.getUserID());
			fail();
		} catch (NotEnoughFoundsException e) {
			fail();
		} catch (WrongAccountHolderException e) {
			assertNotNull(e.getMessage());
		}
		
		try {
			acc1.computeManagementFees(usr1.getUserID());
			fail();
		} catch ( NotEnoughFoundsException e) {
			assertNotNull(e.getMessage());
		} catch (WrongAccountHolderException e) {
			fail();
		}

	}
}
