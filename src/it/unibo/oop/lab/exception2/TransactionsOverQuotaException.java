package it.unibo.oop.lab.exception2;

public class TransactionsOverQuotaException extends IllegalStateException {

	private static final long serialVersionUID = -2274196199036712870L;

	public TransactionsOverQuotaException() {
		// TODO Auto-generated constructor stub
	}

	public TransactionsOverQuotaException(String s) {
		super(s);
		// TODO Auto-generated constructor stub
	}

	public TransactionsOverQuotaException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public TransactionsOverQuotaException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
