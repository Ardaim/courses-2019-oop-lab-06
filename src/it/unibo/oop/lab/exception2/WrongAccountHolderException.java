package it.unibo.oop.lab.exception2;

public class WrongAccountHolderException extends IllegalArgumentException {

	private static final long serialVersionUID = 944577823094791111L;

	public WrongAccountHolderException() {
		// TODO Auto-generated constructor stub
	}

	public WrongAccountHolderException(String s) {
		super(s);
		// TODO Auto-generated constructor stub
	}

	public WrongAccountHolderException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public WrongAccountHolderException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
